var clinic_cell = searchColumns("hospital");
var start_date_cell = searchColumns("Start Date");
var registration_invite = searchColumns("Registration Invite sent");
var registration_form = searchColumns("Registration Form Completed");
var date_group_id = searchColumns("Date Group ID indicated");
var group_id = searchColumns("Group ID");
var api_connection_status = searchColumns("API Connection status");
var booking_link = searchColumns("Booking Link Sent for Testing");

var to_do = "";
var pending = "";
var overdue = "";

function checkEdits(){
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var event1 = ss.getActiveCell().getA1Notation().substring(0, 1) == start_date_cell.substring(0, 1);
  var event2 = (ss.getActiveCell().getValue() != null) && (ss.getActiveCell().getValue() != "");
  var row = ss.getActiveCell().getA1Notation().substring(1);
  var body = "";
  if(event1 && event2){
    body += "\nCell " + ss.getActiveCell().getA1Notation() + " edited by " + Session.getActiveUser().getEmail() + "\nPlease register " + ss.getRange(clinic_cell + row).getValue(); 
  }
  var event3 = ss.getActiveCell().getA1Notation().substring(0, 1) == group_id;
  if(event3 && event2){
    var body = "\nCell " + ss.getActiveCell().getA1Notation() + " edited by " + Session.getActiveUser().getEmail() + "\nGroup ID " + ss.getActiveCell().getValue() + " added" + "\nClinic - " + ss.getRange(clinic_cell + row).getValue();
    body += "\nPlease add widget code, booking URL, site hash, FB link and GMB link";
  }
  Logger.log(body);
  sendEmail(body);
}

function sendEmail(body){
  GmailApp.sendEmail("support@vetstoria.com", "Spreadsheet Update " + new Date(), body);
}  

function getDate(){
  return Utilities.formatDate(new Date(), "GMT+5:30", "dd/MM/yyyy");
}

function checkDaily(){
  doDaily("complete pre-registration", "Clinic", 1, start_date_cell, clinic_cell);
  doDaily("complete registration", "Clinic", 4, start_date_cell, clinic_cell);
  doDaily("complete API congiguration and Send Booking Link", "Group ID", 1, date_group_id, group_id);
  doDaily("follow up on testing", "Group ID", 1, booking_link, group_id);
  doDaily("check the Account Handover Agreed date with the clinic", "Group ID", 3, booking_link, group_id);
}

function doDaily(process, who, day_after, watch_cell, action_cell){
  var ss = SpreadsheetApp.getActiveSpreadsheet();  
  var body = "";
  for (var row = 8; true; row++) {
    var val = ss.getRange(watch_cell + row).getValue();
    if(val == null || val == ""){
      break; 
    }   
    var start_date = new Date(val);
    start_date.setDate(start_date.getDate()+day_after);
    var due_date = Utilities.formatDate(start_date, "GMT+5:30", "dd/MM/yyyy");
    if(due_date == getDate()){
      body += "\nPlease " + process + " for " + who + " " + ss.getRange(action_cell + row).getValue() + "\nClinic - " + ss.getRange(clinic_cell + row).getValue(); 
    }
  }
  Logger.log(body);
  sendEmail(body);
}

function getTime(){
  return Utilities.formatDate(new Date(), "GMT+5:30", "hh:mm a");
}

function dailyReport(){
  var current_time = getTime();
  if(current_time == "10:00 AM" || current_time == "06:00 PM"){
    run_registration();
    send();
    run_configuration();
    send();
  }
}

function send(){
  var body = "<br>Date - " + getDate() + "<br><br>ToDo<br>-----------------------<br><p style='background-color:#3cad3a'>" + to_do + "</p><br>Needs Attention<br>-----------------------<br><p style='background-color:#fc8f00'>" + pending + "</p><br>Overdue<br>-----------------------<br><p style='background-color:#ff6528'>" + overdue + "</p>";  
  sendReport(body);
  Logger.log(body);
  to_do = "";
  pending = "";
  overdue = "";
}

function run_registration(){  
  var ss = SpreadsheetApp.getActiveSpreadsheet();  
  for (var row = 8; true; row++) {
    var clinic = ss.getRange(clinic_cell + row).getValue();
    var val_i = ss.getRange(start_date_cell + row).getValue();
    var val_j = ss.getRange(registration_invite + row).getValue();
    var val_k = ss.getRange(registration_form + row).getValue();
    if (val_i == null || val_i == "") {
        break;
    }
    if (val_k != null && val_k != "") {
      continue;
    }
    
    var event = val_j == null || val_j == "";
    var start_date = new Date(val_i);
    var pending_date = new Date();
    pending_date.setDate(start_date.getDate() + 1);
    pending_date = Utilities.formatDate(pending_date, "GMT+5:30", "dd/MM/yyyy");
    var due_date = new Date();
    due_date.setDate(start_date.getDate() + 4);
    due_date = Utilities.formatDate(due_date, "GMT+5:30", "dd/MM/yyyy");
    var today = Utilities.formatDate(new Date(), "GMT+5:30", "dd/MM/yyyy"); 
    var action = event ? "must do pre-registration" : "must complete registration";
    if (compareDates(today, pending_date) == 'equal' || (!event && compareDates(today, due_date) == "equal")) {
        pending += clinic + " - " + action + "<br>";
    } else if (compareDates(today, pending_date) || (!event && compareDates(today, due_date))) {
        to_do += clinic + " - " + action + "<br>";
    } else {
        overdue += clinic + " - " + action + "<br>";
    }
  }  
}

function run_configuration(){
  var ss = SpreadsheetApp.getActiveSpreadsheet();  
  for (var row = 8; true; row++) {
    var clinic = ss.getRange(clinic_cell + row).getValue();
    var val_l = ss.getRange(date_group_id + row).getValue();
    var val_m = ss.getRange(group_id + row).getValue();
    var val_n = ss.getRange(api_connection_status + row).getValue();
    var val_o = ss.getRange(booking_link + row).getValue();
    if (val_l == null || val_l == "") {
        break;
    }
    if (val_o != null && val_o != "") {
      continue;
    }    
    var event = val_n == null || val_n == "";
    var group_id_date = new Date(val_l);
    var pending_date = new Date();
    pending_date.setDate(group_id_date.getDate() + 1);
    pending_date = Utilities.formatDate(pending_date, "GMT+5:30", "dd/MM/yyyy");
    var today = Utilities.formatDate(new Date(), "GMT+5:30", "dd/MM/yyyy");   
    var action = event ? "must configure API" : "must send Booking Link for testing";
    if (compareDates(today, pending_date) == 'equal') {
        pending += clinic + " - " + action + "<br>";
    } else if (compareDates(today, pending_date)) {
        to_do += clinic + " - " + action + "<br>";
    } else {
        overdue += clinic + " - " + action + "<br>";
    }
  }
}

function compareDates(s_date, e_date){
  var s_day = s_date.substring(0, 2);
  var s_month = s_date.substring(3, 5);
  var s_year = s_date.substring(6);
  var e_day = e_date.substring(0, 2);
  var e_month = e_date.substring(3, 5);
  var e_year = e_date.substring(6);
  if(s_year < e_year){
    return true;
  }else if(s_year == e_year){
    if(s_month < e_month){
      return true; 
    }else if(s_month == e_month){
      if(s_day < e_day){
        return true; 
      }else if(s_day == e_day){
        return "equal"; 
      }
    }
  }
  return false;
}

function sendReport(body){
  MailApp.sendEmail({
    to: "support@vetstoria.com",
    subject: "Spreadsheet Update " + new Date(),
    htmlBody: body
  }); 
}

function demo(){
  Logger.log(searchColumns("hospital"));
  Logger.log(searchColumns("Start Date"));
  Logger.log(searchColumns("Registration Invite sent"));
  Logger.log(searchColumns("Registration Form Completed"));
  Logger.log(searchColumns("Date Group ID indicated"));
  Logger.log(searchColumns("Group ID"));
  Logger.log(searchColumns("API Connection status"));
  Logger.log(searchColumns("Booking Link Sent for Testing"));
}

function searchColumns(search_term){
  var alphabets = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
  var ss = SpreadsheetApp.getActiveSpreadsheet();  
  search_term = filter(search_term, "\n");
  for(var i = 0; i < alphabets.length; i++){
    var val = ss.getRange(alphabets[i] + "1").getValue().toLowerCase();
    val = filter(val, "\n");
    if(search_term == val.substring(0, search_term.length)){
      return alphabets[i]; 
    }
  }
}

function filter(main, sub){
  main = main.split(sub);
  var fmain = "";
  for(var i = 0; i < main.length; i++){
    fmain += main[i]; 
  }
  return fmain.toLowerCase();
}

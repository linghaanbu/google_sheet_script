var columns = ["J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG"];
var green = "#00ff00";
var yellow = "#ff9900";
var red = "#ff0000";
var start_row = 8;
var limit_row = 984;

function countOnBGColor() {
  try { 
     var ss = SpreadsheetApp.getActive().getSheets()[1];
     for(var i = 0; i < columns.length; i++){    
       var column =  columns[i];
       var count_green = getCounti(column, green);
       var count_yellow = getCounti(column, yellow);
       var count_red = getCounti(column, red);     
    
       ss.getRange(column + 4).setValue(count_green);
       ss.getRange(column + 4).setFontColor(green);
       ss.getRange(column + 5).setValue(count_yellow);
       ss.getRange(column + 5).setFontColor(yellow);
       ss.getRange(column + 6).setValue(count_red);
       ss.getRange(column + 6).setFontColor(red);
     }
  } catch(e) {
    //Catch any error here. Example below is just sending an email with the error.
    Logger.log(e);
  }
 
}

function getCounti(column, color_ref){
  var ss = SpreadsheetApp.getActive().getSheets()[1];
  var count = 0;
  var values = ss.getRange(column+start_row+":"+column+limit_row).getBackgrounds();
  for(var x = 0; x < values.length; x++){
    for(var y = 0; y < values[x].length; y++){
      var color_val = values[x][y];
      if(color_ref == color_val){
        count++; 
      }
    }
  }
  return count;
}
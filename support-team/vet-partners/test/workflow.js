var email_address = "lingha@vetstoria.com";
var start_date_column = searchColumns("Start Date");//R
var basic_requirement_column = searchColumns("Basic requirement accepted");//S
var cherry_column = searchColumns("OK by Gareth/Cheryl");//T
var marketing_column = searchColumns("OK by marketing");//U
var it_column = searchColumns("OK by IT");//V
var api_column = searchColumns("API available");//Y
var registration_form_column = searchColumns("Registration form completed");//X
var booking_link_column = searchColumns("Booking Link Sent for Testing\n(Date)");//AA
var branch_column = searchColumns("Branch Name");//D
var clinic_column = searchColumns("Hospital");//C

function checkEdits(){
  try {
    var ss = SpreadsheetApp.getActive().getSheets()[1];
    var active_cell = ss.getActiveCell();
    var branch_name = branch_column + getRow(active_cell.getA1Notation());
    var body = "";
    if(checkEvent1(active_cell.getA1Notation(), ss)){ 
      body += "Send Registration form,";
    }
    if(checkEvent2(active_cell.getA1Notation(), ss)){
      body += " Send Booking Link,";
    }
    if(checkEvent3(active_cell.getA1Notation(), ss)){
      body += " Add widget code, booking link, FB/GMB and other,";
    }
    if(body != ""){
      body += " for Branch - " + ss.getRange(branch_name).getValue() + ", in Clinic - " + ss.getRange(getHospital(branch_name, ss)).getValue() + "\n";
      sendEmail(body);
    }
  }
  catch(e){
    sendError("lingha@vetstoria.com", e);
  }
}

function checkEvent1(active_cell, ss){
  var row = getRow(active_cell.getA1Notation());
  var r_column = start_date_column + row;
  var s_column = basic_requirement_column + row;
  var t_column = cherry_column + row;
  var u_column = marketing_column + row;
  var v_column = it_column + row;
  var r_value = ss.getRange(r_column).getValue();
  var s_value = ss.getRange(s_column).getValue();
  var t_value = ss.getRange(t_column).getValue();
  var u_value = ss.getRange(u_column).getValue();
  var v_value = ss.getRange(v_column).getValue();
  var r_bool = r_value != "" && r_column != null;
  var s_bool = s_value != "" && s_column != null;
  var t_bool = t_value != "" && t_column != null;
  var u_bool = u_value != "" && u_column != null;
  var v_bool = v_value != "" && v_column != null;
  return r_bool && s_bool && t_bool && u_bool && v_bool;
}

function checkEvent2(active_cell, ss){
  var row = getRow(active_cell.getA1Notation());
  var x_column = registration_form_column + row;
  var y_column = api_column + row;
  var x_value = ss.getRange(x_column).getValue();
  var y_value = ss.getRange(y_column).getValue();
  var x_bool = x_value != "" && x_column != null;
  var y_bool = y_value != "" && y_column != null;
  return x_bool && y_bool;
}

function checkEvent3(active_cell, ss){
  var row = getRow(active_cell.getA1Notation());
  var aa_column = booking_link_column + row;
  var aa_value = ss.getRange(aa_column).getValue();
  var aa_bool = aa_value != "" && aa_column != null;
  return aa_bool;
}

function getHospital(branchRange, ss){
  var clinic_cell = clinic_column + getRow(branchRange);
  while(ss.getRange(clinic_cell).getValue() == "" || ss.getRange(clinic_cell).getValue() == null){
    var new_row = parseInt(getRow(branchRange)) - 1;
    Logger.log(new_row);
    clinic_cell = clinic_column + new_row;
    if(new_row < 0){
      return "Hospital not found";
    }
  }
  return clinic_cell;
}

function getRow(a1_notation){
  var numbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
  if(arrayHas(numbers, a1_notation[1])){ return a1_notation.substr(1, a1_notation.length - 1); }
  return a1_notation.substr(2, a1_notation.length - 1);
}

function arrayHas(arr, str){
  for(i = 0; i < arr.length; i++){
    if(arr[i] == str) return true;
  }
  return false;
}

function sendEmail(body){
  GmailApp.sendEmail(email_address, "Spreadsheet Update " + new Date(), body);
}  

function getDate(){
  return Utilities.formatDate(new Date(), "GMT+5:30", "dd/MM/yyyy");
}

function sendError(email, body){
  GmailApp.sendEmail(email, "Errors logged in Test Vet Partners Tracker", body);
}

function searchColumns(search_term){
  try{
    var alphabets = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA"];
    var ss = SpreadsheetApp.getActive().getSheets()[1]; 
    search_term = filter(search_term, "\n");
    for(var i = 0; i < alphabets.length; i++){
      var val = ss.getRange(alphabets[i] + "1").getValue().toLowerCase();
      val = filter(val, "\n");
      if(search_term == val.substring(0, search_term.length)){
        return alphabets[i]; 
      }
    }
  }catch(e){
    Logger.log(e);
  }
}

function filter(main, sub){
  main = main.split(sub);
  var fmain = "";
  for(var i = 0; i < main.length; i++){
    fmain += main[i]; 
  }
  return fmain.toLowerCase();
}
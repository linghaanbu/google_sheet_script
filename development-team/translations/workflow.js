var email = "eranga@vetstoria.com";
var changed_files = [];
var firebaseUrl = "https://translations-6e57f.firebaseio.com/";
var secret = "sh1MCCZqSjRWAlMiWQAAAlfOBdvtSyfWm9OmihED";

//monitoring changes/updates in "Translations (OABP)" spreadsheet in the "Translations" google drive folder
function checkEditsInSpreadsheet() {
  try{
    
    var ss = SpreadsheetApp.getActiveSheet();
    var active_cell = ss.getActiveCell().getA1Notation();
    
    var subject = "Updates at Translations Spreadsheet @ " + Utilities.formatDate(new Date(), "GMT+1", "dd/MM/yyyy");
    var body = "Cell " + active_cell + " changed";
    sendEmail(email, subject, body);
    
  } catch(e){
    sendEmail("lingha@vetstoria.com", "Error from script in translations spreadsheet", "The following errors were logged - " + e);
  }
}

//monitoring changes/updates in "templates folder" in "Translations" google drive folder
function checkEditsInTemplates(){
  try{
    
    var parent_folder = DriveApp.getFoldersByName("Translations").next().getFoldersByName("templates").next();
    getUpdates(parent_folder);
    var subject = "Updates in Templates Folder @ " + Utilities.formatDate(new Date(), "GMT+1", "dd/MM/yyyy");
    var body = "These files were updated - \n";
    for(i = 0; i < changed_files.length; i++){
      body += changed_files[i] + "\n";
    }    
    if(changed_files.length != 0){
      sendEmail(email, subject, body);
    }
    changed_files = [];
    
  } catch(e){
    sendEmail("lingha@vetstoria.com", "Error from script in templates folder", "The following errors were logged - " + e);
  }
}

//sending email to user
function sendEmail(emailAddress, subject, body){
  MailApp.sendEmail(emailAddress, subject, body);
}

//get the updated files
function getUpdates(parent_folder){
  if(parent_folder.getFiles().hasNext()){
    getAllFiles(parent_folder);  
  }
  var child_folders = parent_folder.getFolders();
  while(child_folders.hasNext()){
    try{
      var baby_folder = child_folders.next();
      recursion(baby_folder);
    }
    catch(e) {
      Logger.log(e);
    }
  }
}

function getAllFiles(folder){
  var files = folder.getFiles();
  while(files.hasNext()){
  try{
      var file = files.next();      
      var file_id = file.getId();
      var lastUpdated_firebase = getFromFirebase(file_id);
      var lastUpdated_current = lastUpdatedOn(file_id);
      if(lastUpdated_firebase != null && lastUpdated_current > lastUpdated_firebase){
        changed_files.push(file.getName());
      }
      updateToFirebase(file_id, lastUpdated_current);
  }
  catch(e){ Logger.log(e); }
  }
}

//get lastupdated timestamp for file
function lastUpdatedOn(file_id) {
  return DriveApp.getFileById(file_id).getLastUpdated().getTime();
}

//adding/updating records to firebase
function updateToFirebase(file_id, last_updated){
  var base = FirebaseApp.getDatabaseByUrl(firebaseUrl, secret);
  if(getFromFirebase(file_id) === null){
    //adding a non existing record to firebase
    base.setData(file_id, {last_updated: last_updated});
  }
  else{
    //updating an existing record to firebase
    base.updateData(file_id, {last_updated: last_updated});
  }  
}

//get records from firebase
function getFromFirebase(file_id){
  var base = FirebaseApp.getDatabaseByUrl(firebaseUrl, secret);
  var last_updated = base.getData(file_id);
  return last_updated;
}

function okFunction() {
 Logger.log("working");
 recursion("attributes");
}

function getFromAttributes(){
 var parent_folder = DriveApp.getFoldersByName("attributes").next();
 recursion(parent_folder);
}

function recursion(parent_folder){
 Logger.log(parent_folder.getName());
 if(parent_folder.getFiles().hasNext()){
   getAllFiles(parent_folder);  
 }
 var child_folders = parent_folder.getFolders();
 while(child_folders.hasNext()){
   try{
     var baby_folder = child_folders.next();
     recursion(baby_folder);
   }
   catch(e) {
     Logger.log(e);
   }
 }
}

function getAllFiles(folder){
 var files = folder.getFiles();
 while(files.hasNext()){
 try{
     Logger.log(files.next().getName());
 }
 catch(e){ Logger.log(e); }
 }
}

function getTimeStamp(){
 var file = DriveApp.getFilesByName("Dummy_Doc").next();
 Logger.log("Blob : " + file.getBlob());
 Logger.log("Description : " + file.getDescription());
 Logger.log("Last Updated : " + file.getLastUpdated().getTime());
 var date = new Date(); 
 var timestamp = date.getTime();
 Logger.log("Current timestamp : " + timestamp);
 var test = timestamp > file.getLastUpdated();
 Logger.log("final test : " + test);
}

function getFromFirebase(){
 var firebaseUrl = "https://translations-6e57f.firebaseio.com/";
 var secret = "sh1MCCZqSjRWAlMiWQAAAlfOBdvtSyfWm9OmihED";
 var base = FirebaseApp.getDatabaseByUrl(firebaseUrl, secret);
 var contact = base.getData("power3");
 Logger.log(contact);
}

function updateToFirebase(){
 var firebaseUrl = "https://translations-6e57f.firebaseio.com/";
 var secret = "sh1MCCZqSjRWAlMiWQAAAlfOBdvtSyfWm9OmihED";
 var base = FirebaseApp.getDatabaseByUrl(firebaseUrl, secret);
 //this is one way to update the database
 base.setData("power1/geass", "absolute submission limitless");
 //this is another way to updata the database
 base.updateData("power2", {chess_piece:"king", alien1:"celestial sapien"});
 //if you want to add a record that doesn't exist yet
 base.setData("power3", {alien:"tetramand", ability:"strength", chess_piece:"queen"});
}

function writeToFirebase(){
 //create json data
 var powers = {
   geass1: 'absolute obedience limitless',
   alien1: 'galvan',
   magic1: 'atizulth',
   skills1: 'limitless thought'
 };
 var powers1 = {
   geass: 'absolute telepathy limitless',
   alien: 'galvanic metamorph',
   magic: 'creation',
   skills: 'empath'
 };
 var overall = {power1: powers1, power2: powers};
 var firebaseUrl = "https://translations-6e57f.firebaseio.com/";
 var base = FirebaseApp.getDatabaseByUrl(firebaseUrl);
 base.setData("", overall);
}

function createJSONData(){
 //create json data
 var powers = {
   geass: 'absolute obedience limitless',
   alien: 'galvan',
   magic: 'atizulth',
   skills: 'limitless thought'
 };
 // save that JSON to a file
 DriveApp.createFile('powers.json', JSON.stringify(powers));
}

function getJSONData(){
 // define a file iterator
 // that will iterate through all files with the name 'post.json'
 var iter = DriveApp.getFilesByName("powers.json");
 
 // iterate through all the files named 'post.json
 while (iter.hasNext()) {
   
   // define a File object variable and set the Media Tyep
   var file = iter.next();
   var jsonFile = file.getAs('application/json')
   
   // log the contents of the file
   Logger.log(jsonFile.getDataAsString());
 }  
}  